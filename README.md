# ADAndor3 conda recipe

Home: "https://github.com/areaDetector/ADAndor3"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ADAndor3 module
